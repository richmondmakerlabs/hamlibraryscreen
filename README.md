# README #

This needs to be run on an apache server or something

### What is this repository for? ###

* Buscountdown system located in the Ham Library (TW10 UK) 
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Start by installing apache and php5
* Configuration: no necesary
* Dependencies: apache php5
* Database configuration: none
* How to run tests: run the php code in your home directory
* Deployment instructions: copy to your www/var/html folder 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact